# coding: utf-8

from __future__ import absolute_import
import unittest

from flask import json
from six import BytesIO

from openapi_server.models.body import Body  # noqa: E501
from openapi_server.models.body1 import Body1  # noqa: E501
from openapi_server.models.inline_response200 import InlineResponse200  # noqa: E501
from openapi_server.models.inline_response2001 import InlineResponse2001  # noqa: E501
from openapi_server.models.inline_response2002 import InlineResponse2002  # noqa: E501
from openapi_server.models.inline_response2003 import InlineResponse2003  # noqa: E501
from openapi_server.test import BaseTestCase


class TestDefaultController(BaseTestCase):
    """DefaultController integration test stubs"""

    def test_accounts_get(self):
        """Test case for accounts_get

        
        """
        query_string = [('finInstNum', 004),
                        ('tranNum', 12345),
                        ('bodyLimit', 30),
                        ('pageLimit', 7)]
        headers = { 
            'Accept': 'application/json',
        }
        response = self.client.open(
            '/WASPLabs/Cheque_Chain_APIs/1.0/accounts',
            method='GET',
            headers=headers,
            query_string=query_string)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_banks_get(self):
        """Test case for banks_get

        
        """
        query_string = [('bodyLimit', 30),
                        ('pageLimit', 7)]
        headers = { 
            'Accept': 'application/json',
        }
        response = self.client.open(
            '/WASPLabs/Cheque_Chain_APIs/1.0/banks',
            method='GET',
            headers=headers,
            query_string=query_string)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_cheque_get(self):
        """Test case for cheque_get

        
        """
        query_string = [('chequeID', 014),
                        ('tranNum', 12345),
                        ('id', 005),
                        ('accountID', 1234567),
                        ('clientName', John Smith)]
        headers = { 
            'Accept': 'application/json',
        }
        response = self.client.open(
            '/WASPLabs/Cheque_Chain_APIs/1.0/cheque',
            method='GET',
            headers=headers,
            query_string=query_string)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_cheque_post(self):
        """Test case for cheque_post

        
        """
        body1 = {}
        headers = { 
            'Content-Type': 'application/json',
        }
        response = self.client.open(
            '/WASPLabs/Cheque_Chain_APIs/1.0/cheque',
            method='POST',
            headers=headers,
            data=json.dumps(body1),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_cheque_put(self):
        """Test case for cheque_put

        
        """
        body = {}
        headers = { 
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
        response = self.client.open(
            '/WASPLabs/Cheque_Chain_APIs/1.0/cheque',
            method='PUT',
            headers=headers,
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    unittest.main()
