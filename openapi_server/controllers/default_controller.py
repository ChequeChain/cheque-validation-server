import connexion
import six

from openapi_server.models.body import Body  # noqa: E501
from openapi_server.models.body1 import Body1  # noqa: E501
from openapi_server.models.inline_response200 import InlineResponse200  # noqa: E501
from openapi_server.models.inline_response2001 import InlineResponse2001  # noqa: E501
from openapi_server.models.inline_response2002 import InlineResponse2002  # noqa: E501
from openapi_server.models.inline_response2003 import InlineResponse2003  # noqa: E501
from openapi_server import util


def accounts_get(fin_inst_num=None, tran_num=None, body_limit=None, page_limit=None):  # noqa: E501
    """accounts_get

    return accounts information from specific bank server as long as the bank server is part of the cheque chain network # noqa: E501

    :param fin_inst_num: financial institution number
    :type fin_inst_num: str
    :param tran_num: tansit/branch number
    :type tran_num: str
    :param body_limit: the number of accounts per branch returned
    :type body_limit: int
    :param page_limit: the number of pages when return accounts into
    :type page_limit: int

    :rtype: List[InlineResponse2001]
    """
    return 'do some magic!'


def banks_get(body_limit=None, page_limit=None):  # noqa: E501
    """banks_get

    return accounts information from specific bank server as long as the bank server is part of the cheque chain network # noqa: E501

    :param body_limit: the number of accounts per branch returned
    :type body_limit: int
    :param page_limit: the number of pages when return accounts into
    :type page_limit: int

    :rtype: List[InlineResponse200]
    """
    return 'do some magic!'


def cheque_get(cheque_id=None, tran_num=None, id=None, account_id=None, client_name=None):  # noqa: E501
    """cheque_get

    obtian infromation about a cheque from the payor banck and branch, such as if the cheque exist, if it has been tokenized or not, the satus of the cheque and other basic cheque information. # noqa: E501

    :param cheque_id: this is the cheque number taken from the pyhsical cheque
    :type cheque_id: str
    :param tran_num: tansit/branch number
    :type tran_num: str
    :param id: bank or financial insitatuion  id
    :type id: str
    :param account_id: payor account number
    :type account_id: str
    :param client_name: payor full name
    :type client_name: str

    :rtype: InlineResponse2002
    """
    return 'do some magic!'


def cheque_post(body1):  # noqa: E501
    """cheque_post

    enable the payee or the payee&#39;s bank to post a cheque for clearance to the blockchain cheque clearance network # noqa: E501

    :param body1: 
    :type body1: dict | bytes

    :rtype: None
    """
    if connexion.request.is_json:
        body1 = Body1.from_dict(connexion.request.get_json())  # noqa: E501
    return 'do some magic!'


def cheque_put(body):  # noqa: E501
    """cheque_put

    enable the payor or the payor branch to place the tokenization of a cheque to the blockchain cheque clearnace network # noqa: E501

    :param body: 
    :type body: dict | bytes

    :rtype: InlineResponse2003
    """
    if connexion.request.is_json:
        body = Body.from_dict(connexion.request.get_json())  # noqa: E501
    return 'do some magic!'
